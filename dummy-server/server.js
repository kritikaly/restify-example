let restify = require('restify');
const mongoose = require('mongoose');
var bodyParser = require('body-parser');

const User = new mongoose.Schema({
    fullName: String,
    email: String
});
const Users = mongoose.model('User', User, 'Users');

const port = 8888;

const server = restify.createServer();

server.use(restify.plugins.bodyParser());

// End Points


server.listen(port, function () {
    console.log(`Example app listening on port ${ port }`);
    mongoose.connect('mongodb://localhost/tacobell', {useNewUrlParser: true}, function(err) {
        console.log('mongo is up and running!!');
    })
});

server.get('/:id', async (req, res, next) => {
    Users.findById(req.params.id, function(err, user) { 
        console.log(user);
        res.send(user);
    });
});
